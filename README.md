## Pre-requisites

1. Python 3
2. Boto3: pip install boto3
3. Paramiko: pip install paramiko
4. AWS CLI

---
## Deployment Instructions

1. Install the required dependencies
2. Create a new user using the IAM management console and configure credentials using "aws configure"
3. Create a key pair
4. Create a security group
5. Run the master.py file in a terminal, and specify the requested inputs. Please see the examples below.
##
Specify the number of instances: y or n? y

Number of instances: 10

Timeout (s): 500

Difficulty number (bits): 12
##
Specify the number of instances: y or n? n

Choose run time (mins): 4

Timeout (s): 550

Difficulty number (bits): 24