import hashlib
import datetime
import argparse

arg_parse = argparse.ArgumentParser()
arg_parse.add_argument('-c', '--instanceNum', required = True, type = int, help = "Current instance")
arg_parse.add_argument('-n', '--instances', required = True, type = int, help = "Total number of instances")
arg_parse.add_argument('-d', '--diff_bits', required = True, type = int, help = "Difficulty number in bits")


def nonce_vals(instance_num, len_vms):
    const = pow(2, 32) / len_vms
    # print(const)
    const2 = int(const)
    n1 = const2 * instance_num
    # print(n1)
    n2 = const2 * (instance_num + 1)
    # print(n2)
    return n1, n2


def main(args):
    time_started = datetime.datetime.now()
    instance_num = int(args.instanceNum)
    len_vms = int(args.instances)
    diff_bits = int(args.diff_bits)

    block = 'COMSM0010cloud'

    difficulty_str = ''
    for i in range(int(diff_bits / 4)):
        difficulty_str = difficulty_str + '0'

    n1, n2 = nonce_vals(instance_num, len_vms)

    for i in range(n1, n2):
        z = block + str(i)
        dhash = hashlib.sha256(z.encode()).hexdigest()
        dhash2 = hashlib.sha256(dhash.encode()).hexdigest()

        if dhash2.startswith(difficulty_str):
            time_stopped = datetime.datetime.now()
            print("Instance {} was successful".format(instance_num))
            print("Run time: ", time_stopped - time_started)
            print("The golden nonce is ", i)
            exit()
    exit()


if __name__ == '__main__':
    main(arg_parse.parse_args())
