import boto3
import logging
import signal
import threading
import paramiko
import datetime
import time

ec2 = boto3.client('ec2', region_name='us-east-1')


def check_active_vms(id):
    time.sleep(35)
    wait_active = True

    while wait_active:
        instances_list = ec2.describe_instances(InstanceIds = [id])
        checked_instance = instances_list['Reservations'][0]['Instances'][0]

        if checked_instance['State']['Name'] == 'running':
            time.sleep(35)
            return checked_instance
        else:
            time.sleep(15)


def farming_parallel(worker_inputs):
    diff_bits, vm_id, vm_number, len_vms = worker_inputs
    current_inst = check_active_vms(vm_id)
    client = paramiko.SSHClient()
    client_key = paramiko.RSAKey.from_private_key_file('my_key.pem')
    # set the policy for each new instance that's accessed
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname = current_inst['PublicIpAddress'], username = 'ubuntu', pkey = client_key)
    transfer_session = client.open_transfer_session()
    # copy the script on the instance
    transfer_session.put("find_nonce.py", "find_nonce.py")
    worker_cmd = "python3 find_nonce.py -c " + str(vm_number) + " -n " + str(len_vms) + " -d " + str(diff_bits)

    stdin, stdout, stderr = client.exec_command(worker_cmd)
    lns = stdout.readlines()
    for i in lns:
        print(i)
    client.close()

    return


def get_inputs():
    choice = input("Specify the number of instances: y or n? ")
    if choice == 'n':
        input_time = int(input("Choose run time (mins): "))
        timeout_s = int(input("Timeout (s): "))
        diff_bits = int(input("Difficulty number (bits): "))
        instances_N = diff_bits / (input_time * 0.5)
        instances_N = int(round(instances_N))
        if instances_N < 1:
            instances_N = 1
        if instances_N > 30:
            instances_N = 30
    elif choice == 'y':
        instances_N = int(input("Number of instances: "))
        timeout_s = int(input("Timeout (s): "))
        diff_bits = int(input("Difficulty number (bits): "))
    else:
        exit()

    return timeout_s, instances_N, diff_bits


def main():
    timeout_s, instances_N, diff_bits = get_inputs()

    signal_val = signal.signal(signal.SIGALRM, signal_end)
    time_started = datetime.datetime.now()

    vms = ec2.run_instances(
            ImageId = 'ami-00eb20669e0990cb4',
            MinCount = 1,
            MaxCount = instances_N,
            InstanceType = 't2.micro',
            KeyName = 'my_key',
            SecurityGroupIds = ['sg-1e66eb4a']
        )

    all_vms = list( map(lambda x: x['InstanceId'], vms['Instances']) )
    worker_inputs = [(diff_bits,id,all_vms.index(id),len(all_vms)) for id in all_vms]

    try:
        signal.alarm(timeout_s)
        workers = []
        searching = True
        for instance_i in range(instances_N):
            # give the inputs to each thread (difficuty-level, instance id, index of the current instance, total number of instances)
            worker = threading.Thread( target = farming_parallel, args = (worker_inputs[instance_i],) )
            workers.append(worker)
            # boolean value indicating whether this thread is a daemon thread or not
            worker.setDaemon(True)
            # starts the thread
            worker.start()
        while searching:
            time.sleep(2)
            for worker in workers:
                # check if the golden nonce has been found
                if not worker.isAlive():
                    # end the search
                    searching = False
                    break

        try:
            ec2.terminate_instances(InstanceIds = all_vms)
        except ClientError as e :
            logging.error(e)
            return None

        time_stopped = datetime.datetime.now()
        print('Terminating, run time: ', time_stopped - time_started)
        exit()

    # emergency stop and scram
    except KeyboardInterrupt:
        try:
            ec2.terminate_instances(InstanceIds = all_vms)
        except ClientError as e :
            logging.error(e)
            return None
        exit()

    finally:
        # if the user's input time has elapsed, kill all the instaces
        signal.alarm(0)
        signal.signal(signal.SIGALRM, signal_val)
        try:
            ec2.terminate_instances(InstanceIds = all_vms)
        except ClientError as e :
            logging.error(e)
            return None


def signal_end(signum, frame):
    if signum or frame:
        pass
    exit()


if __name__ == '__main__':
    main()
